//
//  StyloItemsDetailViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 21/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloItemsDetailViewController.h"
#import "StyloNavigationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
//#import "UIImage+GIF.h"
#import "HMSegmentedControl.h"
#import "MBProgressHUD.h"
#import "StyloAppDelegate.h"
#import "StyloLookupInventoryViewController.h"

@interface StyloItemsDetailViewController ()
@property (nonatomic,strong) HMSegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation StyloItemsDetailViewController{
    BOOL popUpIsOpen;
    NSMutableArray *quantitiesAvailable;
    NSMutableArray *availableSizes;
    NSMutableArray *sizesId;
    MBProgressHUD *hud;
    StyloAppDelegate *appDelegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
   /* UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: @"NewBack" style: UIBarButtonItemStyleBordered target: self.navigationController action: @selector(backButtonTapped)];
    
   // [[self navigationItem] setBackBarButtonItem: newBackButton];
    self.navigationItem.leftBarButtonItem = newBackButton;
*/
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    _pageControl.transform = CGAffineTransformMakeRotation(M_PI / 2);
    NSMutableDictionary *tempItem = [_item mutableCopy];
    
    NSArray *sizes = [[tempItem objectForKey:@"sizes"] mutableCopy];
    NSArray *sortedArray;
    sortedArray = [sizes sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *firstNumber = [NSNumber numberWithInteger:[[a objectAtIndex:1] integerValue]];
        NSNumber *secondNumber = [NSNumber numberWithInteger:[[b objectAtIndex:1] integerValue]];
        return [firstNumber compare:secondNumber];
    }];
    [tempItem setObject:sortedArray forKey:@"sizes"];
    
    _item = tempItem;
    
    popUpIsOpen = NO;
    //Removing product Id from name
    NSString *name = [_item objectForKey:@"name"];
    NSRange range = [name rangeOfString:@"("];
    
    if (range.location != NSNotFound) {
        _itemName.text = [name substringToIndex:range.location];
    }else {
        _itemName.text = name;
    }
    
    //Setting up image views for different images of item
    for(int i=0; i<[[_item objectForKey:@"images"] count];i++)
    {
        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView setContentMode:UIViewContentModeCenter];
        [imageView setClipsToBounds:YES];
       // imageView.frame = CGRectMake(i*320, 0, 320, self.view.frame.size.height-218);
        //imageView.frame = CGRectMake(i*320, 0, 320, 320);
       // imageView.frame = CGRectMake( 20,i*240, 300, 320);
        imageView.frame = CGRectMake( 20,i*240, _itemScrollView.frame.size.width-40, 240);
        NSString *thumbURL = [[[_item objectForKey:@"images"] objectAtIndex:i] objectForKey:@"url"];
        //[imageView setImageWithURL:[NSURL URLWithString:thumbURL] placeholderImage:[UIImage sd_animatedGIFNamed:@"loading.gif"]];
        [imageView setImageWithURL:[NSURL URLWithString:thumbURL] placeholderAnimationImages:appDelegate.loadingImageArray];
        /*NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        for (int i=1; i<13; i++) {
            NSString *strImgeName = [NSString stringWithFormat:@"loading%02d.png", i];
            UIImage *image = [UIImage imageNamed:strImgeName];
            if (image){
                [tempArray addObject:image];
            }
        }

        [imageView setAnimationImages:tempArray];
        [imageView setAnimationDuration:2];
        [imageView startAnimating];*/
        [_itemScrollView addSubview:imageView];
    }
    
    //Filling up Item Details
    _itemDescription.text = [_item objectForKey:@"short_description"];
    _itemPrice.text = [NSString stringWithFormat:@"%.2f",[[_item objectForKey:@"price"] floatValue]];
    
   // _itemScrollView.contentSize = CGSizeMake(320*[[_item objectForKey:@"images"] count], self.view.frame.size.height-218);
     //_itemScrollView.contentSize = CGSizeMake( self.view.frame.size.width,320*[[_item objectForKey:@"images"] count] );
    _itemScrollView.contentSize = CGSizeMake( self.view.frame.size.width-60,240*[[_item objectForKey:@"images"] count] );
    
    [self setUpSegmentedControl];
    
    [_popupView.layer setPosition:CGPointMake(160, self.view.frame.size.height-28)];
    
    //Adding side menu button
  /*  UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(revealMenu)];
    [self.navigationItem setRightBarButtonItem:barButton];
   */
    
    //Pan Gesture to avoid propagation on popView
    UIPanGestureRecognizer *aPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:nil];
    [_popupView addGestureRecognizer:aPanGestureRecognizer ];
    
    //Setting Up Sizes Segmented Control
    [_sizesSegmentedControl removeAllSegments];
    quantitiesAvailable = [[NSMutableArray alloc]init];
    availableSizes = [[NSMutableArray alloc]init];
    sizesId = [[NSMutableArray alloc]init];
    for (int i=0; i<[[_item objectForKey:@"sizes"] count]; i++) {
        NSNumber *quantity = [NSNumber numberWithInteger:[[[[_item objectForKey:@"sizes"] objectAtIndex:i] objectAtIndex:2] integerValue]];
        NSString *size = [[[_item objectForKey:@"sizes"] objectAtIndex:i] objectAtIndex:1];
        NSString *idForSize = [[[_item objectForKey:@"sizes"] objectAtIndex:i] objectAtIndex:0];
        
        [quantitiesAvailable addObject:quantity];
        [availableSizes addObject:size];
        [sizesId addObject:idForSize];
        [_sizesSegmentedControl insertSegmentWithTitle:size atIndex:i animated:YES];
    }
    
}
-(void)backButtonTapped{

    NSLog(@"back BUTTON PRESSED");
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)lookUpButtonOnTap:(id)sender {
    NSLog(@"tapped");
   StyloLookupInventoryViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"lookUpScene"];
    vc.item = _item;
    
}
-(void)openPopup{
    
    
    popUpIsOpen = YES;
    
    [UIView transitionWithView:self.popupButton duration:0.4 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        
        [_popupButton setTitle:@"       Hide" forState:UIControlStateNormal];
        [_popupButton setImage:[UIImage imageNamed:@"up.png"] forState:UIControlStateNormal];
        
    } completion:nil];
    /* Animation Code*/
    
    CABasicAnimation *popupAnimation;
   
    CGPoint endPoint = CGPointMake(160, self.view.frame.size.height-143);
    popupAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    popupAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
   
    popupAnimation.fromValue = [NSValue valueWithCGPoint:_popupView.layer.position];
    popupAnimation.toValue = [NSValue valueWithCGPoint:endPoint];
    popupAnimation.repeatCount = 1;
    popupAnimation.duration = 0.5;
    [_popupView.layer addAnimation:popupAnimation forKey:@"position"];
    [_popupView.layer setPosition:endPoint];
}
-(void)closePopup{
    
    popUpIsOpen = NO;
    
    [UIView transitionWithView:self.popupButton duration:0.4 options:UIViewAnimationOptionTransitionCurlDown animations:^{
        
        [self.popupButton setTitle:@"       Buy" forState:UIControlStateNormal];
        [_popupButton setImage:[UIImage imageNamed:@"buy.png"] forState:UIControlStateNormal];
        
    } completion:nil];
    
    /* Animation Code*/
    
    CABasicAnimation *popupAnimation;
    
    CGPoint endPoint = CGPointMake(160, self.view.frame.size.height-28);
    popupAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    popupAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    popupAnimation.fromValue = [NSValue valueWithCGPoint:_popupView.layer.position];
    popupAnimation.toValue = [NSValue valueWithCGPoint:endPoint];
    popupAnimation.repeatCount = 1;
    popupAnimation.duration = 0.5;
    
    [_popupView.layer addAnimation:popupAnimation forKey:@"position"];
    [_popupView.layer setPosition:endPoint];
}
-(void)setUpSegmentedControl{
    NSMutableArray *sectionTitles;
    sectionTitles = [[NSMutableArray alloc] init];
    for(int i=0; i<[[_item objectForKey:@"images" ] count] ; i++)
    {
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.frame = CGRectMake(0, 0, 50, 50);
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        //[imageView setImageWithURL:[[[_item objectForKey:@"images"]objectAtIndex:i]objectForKey:@"url"] placeholderImage:[UIImage imageNamed:@"loading.gif"]];
       // [imageView setImageWithURL:[NSURL URLWithString:[[[_item objectForKey:@"images"]objectAtIndex:i]objectForKey:@"url"]] placeholderAnimationImages:appDelegate.loadingImageArray];
        [sectionTitles addObject:imageView];
    }
    
    self.segmentedControl = [[HMSegmentedControl alloc] initWithSectionImageViews:sectionTitles];
    _pageControl.numberOfPages = sectionTitles.count;
    self.segmentedControl.frame=CGRectMake(0, 0, 320, 66);
    self.segmentedControl.selectedSegmentIndex = 0;
    _pageControl.currentPage=0;
    self.segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.segmentedControl.scrollEnabled = YES;
    self.segmentedControl.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    self.segmentedControl.textColor = [UIColor lightGrayColor];
    self.segmentedControl.selectedTextColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.36 green:0.176 blue:0.569 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe; //HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
       // [weakSelf.itemScrollView scrollRectToVisible:CGRectMake( 320 * index,0, 320, weakSelf.view.frame.size.height-198) animated:YES];
        [weakSelf.itemScrollView scrollRectToVisible:CGRectMake( 0,320 * index, 320, 320) animated:YES];
    }];
    [_viewForSegments addSubview:_segmentedControl];

}

-(void)shakeItBaby:(UIView *)viewToShake{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:4];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([viewToShake center].x - 5.0f, [viewToShake center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([viewToShake center].x + 5.0f, [viewToShake center].y)]];
    
    [viewToShake.layer addAnimation:animation forKey:@"key"];
}
#pragma mark - Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    //CGFloat pageWidth = _itemScrollView.frame.size.width;
    //int page = floor((_itemScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    //[_segmentedControl setSelectedSegmentIndex:page animated:YES];
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
   // CGFloat pageWidth = scrollView.frame.size.width;
   // NSInteger page = scrollView.contentOffset.x / pageWidth;
    CGFloat pageHeight= scrollView.frame.size.height;
    NSInteger page = scrollView.contentOffset.y / pageHeight;
    
    [self.segmentedControl setSelectedSegmentIndex:page animated:YES];
    [_pageControl setCurrentPage:page];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - IB Actions
- (IBAction)togglePopup:(id)sender {
    if(popUpIsOpen){
        [self closePopup];
        //[_popupButton setTitle:@"       Buy" forState:UIControlStateNormal];
        
    }else{
        [self openPopup];
        
    }
}

- (IBAction)newSizeSelected:(id)sender {
    if ([_sizesSegmentedControl selectedSegmentIndex]>-1) {
       int maxQuantity =  [[quantitiesAvailable objectAtIndex:[_sizesSegmentedControl selectedSegmentIndex]] intValue];
        for(int i=0; i<5; i++){
            if (i<maxQuantity) {
                [_quantitySegmentedControl setEnabled:YES forSegmentAtIndex:i];
            }else{
                [_quantitySegmentedControl setEnabled:NO forSegmentAtIndex:i];
            }
        }
        [_quantitySegmentedControl setSelectedSegmentIndex:-1];
    }
}



- (IBAction)addToCart:(id)sender {
    NSLog(@"added to cart");
    long quantity;
 /*   if ([_sizesSegmentedControl selectedSegmentIndex]<0) {
        [self shakeItBaby:_sizesSegmentedControl];
    }else {
        quantity =[_quantitySegmentedControl selectedSegmentIndex]+1;
        if(quantity==0){
            [self shakeItBaby:_quantitySegmentedControl];
        }else{
  */
          /* /// THIS !!!
           
           NSURL *targetURL = [NSURL URLWithString:@"http://54.179.143.160/addProduct.php"];
            NSMutableURLRequest *addProductRequest = [[NSMutableURLRequest alloc] initWithURL:targetURL];
            [addProductRequest setHTTPMethod:@"POST"];
            NSString *productData = [NSString stringWithFormat:@"id=%@&qty=%li",[sizesId objectAtIndex:[_sizesSegmentedControl selectedSegmentIndex]], quantity];
            NSString *dataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[productData length]];
            NSData *theData = [productData dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            
            [addProductRequest setValue:dataLength forHTTPHeaderField:@"Content-Length"];
            [addProductRequest setHTTPBody:theData];
            
            NSURLConnection *addProductConnection = [NSURLConnection connectionWithRequest:addProductRequest delegate:self];
            [addProductConnection start];
            
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Adding to cart";
           ///--->>till THIS */
            
      //  }
   // }
}

- (IBAction)shareIt:(id)sender {
    UIImage *imageToshare ;
    UIImageView *imageView = (UIImageView *)[[_itemScrollView subviews] objectAtIndex:0];
    imageToshare = imageView.image;
    NSString *stringToShare = [NSString stringWithFormat:@"Check out this shoe from stylo iPhone app\n #styloapp\n"];
    NSURL *urlToShare = [NSURL URLWithString:@"http://www.styloshoes.com"];
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[imageToshare,urlToShare,stringToShare] applicationActivities:nil];
    [self presentViewController:controller animated:YES completion:nil];
    
}
#pragma mark - Connection Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    //[self.data removeAllObjects];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)d {
 
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
    
    [hud hide:YES];
    if ([[response objectForKey:@"code"]  isEqual:@"100"]) {
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
        
        // Set custom view mode
        hud.mode = MBProgressHUDModeCustomView;
        
        hud.labelText = @"Completed";
        [hud hide:YES afterDelay:1];
        
        [self closePopup];
        
        [[[UIAlertView alloc] initWithTitle:@"Product Added" message:[response objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil] show];
        
    }else{
        [hud hide:YES];
        [[[UIAlertView alloc] initWithTitle:@"Error Adding Product" message:[response objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil] show];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
    [hud hide:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // Do anything you want with it
    
}


@end
