//
//  StyloConfirmOrderViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 12/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloConfirmOrderViewController.h"
#import "MBProgressHUD.h"
#import "StyloThankYouViewController.h"
#import "StyloAppDelegate.h"

@interface StyloConfirmOrderViewController ()
@property (strong, nonatomic) IBOutlet UILabel *billingName;
@property (strong, nonatomic) IBOutlet UILabel *billingAddress;
@property (strong, nonatomic) IBOutlet UILabel *shippingName;
@property (strong, nonatomic) IBOutlet UILabel *shippingAddress;
@property (strong, nonatomic) IBOutlet UILabel *grandTotal;

@end

@implementation StyloConfirmOrderViewController{
    MBProgressHUD *hud;
    NSURLConnection *checkoutConnection;
    StyloAppDelegate *appDelegate;
    NSMutableDictionary *dataForCheckout;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    
    dataForCheckout = [_checkoutData mutableCopy];
    [self.navigationItem setTitle:@"Confirm Checkout"];
    
    _billingName.text = [NSString stringWithFormat:@"%@ %@",[dataForCheckout objectForKey:@"fname"],[dataForCheckout objectForKey:@"lname"]];
    _billingAddress.text = [NSString stringWithFormat:@"%@, %@",[dataForCheckout objectForKey:@"address"],[dataForCheckout objectForKey:@"city"]];
    
    _shippingName.text = [NSString stringWithFormat:@"%@ %@",[dataForCheckout objectForKey:@"Sfname"],[dataForCheckout objectForKey:@"Slname"]];
    _shippingAddress.text = [NSString stringWithFormat:@"%@, %@",[dataForCheckout objectForKey:@"Saddress"],[dataForCheckout objectForKey:@"Scity"]];
    
    if ([dataForCheckout objectForKey:@"sameShipping"]==[NSNumber numberWithBool:YES]) {
        _shippingAddress.text = [dataForCheckout objectForKey:@"Saddress"];
        [dataForCheckout removeObjectsForKeys:@[@"Sfname",@"Slname",@"Saddress",@"Scity",@"Sphone"]];
    }
    
    [_billingAddress sizeToFit];
    [_shippingAddress sizeToFit];
    
    _grandTotal.text = [NSString stringWithFormat:@"Rs. %@",[dataForCheckout objectForKey:@"cartTotal"]];
    
    if ([dataForCheckout objectForKey:@"signup"]==[NSNumber numberWithBool:NO]) {
        [dataForCheckout removeObjectsForKeys:@[@"password1", @"password2"]];
    }
    
    if (!appDelegate.isLoggedIn) {
        [dataForCheckout removeObjectsForKeys:@[@"saveAddressBill", @"saveAddressShip"]];
    }
    
    [dataForCheckout removeObjectForKey:@"cartTotal"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Connection Methods
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)d {
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"response: %@", response);
    [hud hide:YES];
    
    if ([[response objectForKey:@"code"] isEqualToString:@"100"]) {
        StyloThankYouViewController *thanks = [self.storyboard instantiateViewControllerWithIdentifier:@"thankyouPage"];
        thanks.orderId = [response objectForKey:@"order_id"];
        [self presentViewController:thanks animated:YES completion:^{
            [self.navigationController popToRootViewControllerAnimated:NO];
        }];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [hud hide:YES];
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
}
#pragma mark - Interface Builder actions
- (IBAction)completeCheckout:(id)sender {
    NSLog(@"completing purhcase");
    
    NSMutableString *requestString = [NSMutableString stringWithString:@""];
    
    NSURL *checkOutUrl = [NSURL URLWithString:@"http://54.179.143.160/checkout.php"];
    
    NSMutableURLRequest *checkoutRequest = [NSMutableURLRequest requestWithURL:checkOutUrl cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:15.0];
    int i=0;
    for (NSString* key in dataForCheckout){
        if (i==0) {
            [requestString appendString:[NSString stringWithFormat:@"%@=%@",key,[dataForCheckout objectForKey:key]]];
            i++;
        }else{
            [requestString appendString:[NSString stringWithFormat:@"&%@=%@",key,[dataForCheckout objectForKey:key]]];
        }
    }
    NSLog(@"Request string is: %@",requestString);
    
    NSData *postData = [requestString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [checkoutRequest setHTTPMethod:@"POST"];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[requestString length]];
    [checkoutRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [checkoutRequest setHTTPBody:postData];
    
    
    checkoutConnection = [[NSURLConnection alloc] initWithRequest:checkoutRequest delegate:self];
    
    
    [checkoutConnection start];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText  = @"Completing Purchase";
}


@end
