//
//  StyloMapViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 23/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface storeLocation : NSObject <MKAnnotation>

- (id)initWithCity:(NSString*)city name:(NSString*)name coordinate:(CLLocationCoordinate2D)coordinate;
- (MKMapItem*)mapItem;

@end

@interface StyloMapViewController : UIViewController <MKMapViewDelegate>
- (IBAction)cancel:(id)sender;
- (IBAction)toggleView:(id)sender;

@property (nonatomic,strong) NSDictionary *selectedStore;

@property (strong, nonatomic) IBOutlet UIButton *toggleButton;
@property (strong, nonatomic) IBOutlet UILabel *city;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *address;
@property (strong, nonatomic) IBOutlet UILabel *distance;
@property (strong, nonatomic) IBOutlet UIView *descriptionView;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (strong, nonatomic) IBOutlet UIButton *phone;
@property (strong, nonatomic) IBOutlet UIButton *cell;

@property (nonatomic,strong) NSArray *stores;
@end
