//
//  StyloItemsViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloItemsViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIPageViewControllerDataSource,UIPageViewControllerDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic,strong) NSMutableArray *styloData;
@property (strong, nonatomic) IBOutlet UIView *viewForPageView;
@property (strong, nonatomic) IBOutlet UIView *viewForSegments;
@property (strong, nonatomic) NSString *targetURL;

-(void)moveSegmentedControlToindex:(NSUInteger)index;
@end
