//
//  StyloThankYouViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 13/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloThankYouViewController : UIViewController

@property (nonatomic, strong) NSString *orderId;

@end
