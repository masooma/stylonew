//
//  StyloViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
