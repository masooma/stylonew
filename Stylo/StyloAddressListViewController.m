//
//  StyloshippingInformationViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 09/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloAddressListViewController.h"
#import "MBProgressHUD.h"
#import "StyloAppDelegate.h"
#import "StyloAddressFormViewController.h"
#import "StyloNavigationViewController.h"
#import "StyloConfirmOrderViewController.h"

@interface StyloAddressListViewController ()

@end

@implementation StyloAddressListViewController{
    MBProgressHUD *hud;
    NSMutableArray *addresses;
    NSString *email;
    StyloAppDelegate *appDelegate;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Shipping Address"];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    
    if (appDelegate.isLoggedIn) {
        NSURL *url = [NSURL URLWithString:@"http://54.179.143.160/checkoutForm.php"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [connection start];
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setLabelText:@"Fetching addresses"];
    }
    
    
    addresses = [[NSMutableArray alloc] init];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:nil action:nil];
    [self.view addGestureRecognizer:panRecognizer];
    [self.navigationController.navigationBar addGestureRecognizer:panRecognizer];
    
    if (_isShipping) {
        [self.navigationItem setTitle:@"Shipping Information"];
       /* NSDictionary *address = [[NSDictionary alloc]
                                 initWithObjects:@[[_checkoutData objectForKey:@"fname"], [_checkoutData objectForKey:@"lname"], [_checkoutData objectForKey:@"phone"], [_checkoutData objectForKey:@"city"], [_checkoutData objectForKey:@"address"]]
                                 forKeys:@[@"firstname", @"lastname", @"telephone", @"city", @"street"]];*/
        NSDictionary *address = [[NSDictionary alloc] initWithObjects:@[@"Same", @"As Billing", @"", @"", @"Same as Billing"]
                                 forKeys:@[@"firstname", @"lastname", @"telephone", @"city", @"street"]];
        [addresses addObject:address];
    }else{
        [self.navigationItem setTitle:@"Billing Information"];
    }
    
    UIBarButtonItem *addAddressButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewAddress)];
    [self.navigationItem setRightBarButtonItem:addAddressButton];
    
}

-(void)addNewAddress{
    StyloAddressFormViewController *addressForm = [self.storyboard instantiateViewControllerWithIdentifier:@"billingInfoView"];
    addressForm.isShipping = _isShipping;
    addressForm.checkoutData = _checkoutData;
    [self.navigationController pushViewController:addressForm animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table View Data Source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [addresses count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (_isShipping && (indexPath.row == 0)) {
        
        return [tableView dequeueReusableCellWithIdentifier:@"sameCell"];
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addressCell"];
    
    NSDictionary *address = [addresses objectAtIndex:indexPath.row];
    
    UILabel *nameLabel  = (UILabel *)[cell viewWithTag:1];
    UILabel *addressLabel = (UILabel *)[cell viewWithTag:2];
    
    nameLabel.text = [NSString stringWithFormat:@"%@ %@", [address objectForKey:@"firstname"], [address objectForKey:@"lastname"]];
    
    addressLabel.text = [NSString stringWithFormat:@"%@, %@",[address objectForKey:@"street"],[address objectForKey:@"city"]];
    
    return cell;
}
#pragma mark - Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *selectedAddress = [addresses objectAtIndex:indexPath.row];
    if (_isShipping) {
        if (indexPath.row == 0) {
            [_checkoutData setObject:[NSNumber numberWithBool:YES] forKey:@"sameShipping"];
        }else{
            [_checkoutData setObject:[NSNumber numberWithBool:NO] forKey:@"sameShipping"];
        }
        [_checkoutData setObject:[selectedAddress objectForKey:@"firstname"] forKey:@"Sfname"];
        [_checkoutData setObject:[selectedAddress objectForKey:@"lastname"] forKey:@"Slname"];
        [_checkoutData setObject:[selectedAddress objectForKey:@"telephone"] forKey:@"Sphone"];
        [_checkoutData setObject:[selectedAddress objectForKey:@"street"] forKey:@"Saddress"];
        [_checkoutData setObject:[selectedAddress objectForKey:@"city"] forKey:@"Scity"];
        
        StyloConfirmOrderViewController *confirmPage = [self.storyboard instantiateViewControllerWithIdentifier:@"confirmPage"];
        confirmPage.checkoutData = _checkoutData;
        [self.navigationController pushViewController:confirmPage animated:YES];
    }else{
        
        [_checkoutData setObject:[selectedAddress objectForKey:@"firstname"] forKey:@"fname"];
        [_checkoutData setObject:[selectedAddress objectForKey:@"lastname"] forKey:@"lname"];
        if ([selectedAddress objectForKey:@"email"]) {
            [_checkoutData setObject:[selectedAddress objectForKey:@"email"] forKey:@"email"];
        }else{
            [_checkoutData setObject:email forKey:@"email"];
        }
        
        [_checkoutData setObject:[selectedAddress objectForKey:@"telephone"] forKey:@"phone"];
        [_checkoutData setObject:[selectedAddress objectForKey:@"street"] forKey:@"address"];
        [_checkoutData setObject:[selectedAddress objectForKey:@"city"] forKey:@"city"];
        
        StyloAddressListViewController *shippingAddressPage = [self.storyboard instantiateViewControllerWithIdentifier:@"shippingView"];
        shippingAddressPage.isShipping = YES;
        shippingAddressPage.checkoutData = _checkoutData;
        [self.navigationController pushViewController:shippingAddressPage animated:YES];
    }
}
#pragma mark - Connection Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    //[self.data removeAllObjects];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)d {

    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
    
    email = [[response objectForKey:@"info"] objectForKey:@"email"];
    [addresses addObjectsFromArray:[response objectForKey:@"addresses"]];
    
    [hud hide:YES];
    if(([addresses count] == 0) && !_isShipping){
        StyloAddressFormViewController *newAddress = (StyloAddressFormViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"billingInfoView"];
        newAddress.isShipping = NO;
        newAddress.checkoutData = _checkoutData;
        [self.navigationController performSelector:@selector(replaceLastWith:) withObject:newAddress];
    }else{
        [_addressesTableView reloadData];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
}

#pragma mark - Interface Builder Actions

- (IBAction)addNewAddress:(id)sender {
    [self addNewAddress];
}

@end
