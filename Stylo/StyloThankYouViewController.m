//
//  StyloThankYouViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 13/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloThankYouViewController.h"

@interface StyloThankYouViewController ()
@property (strong, nonatomic) IBOutlet UILabel *trackingId;

@end

@implementation StyloThankYouViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _trackingId.text = _orderId;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)continue:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
