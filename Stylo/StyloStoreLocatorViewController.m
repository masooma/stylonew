//
//  StyloStoreLocatorViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 22/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloStoreLocatorViewController.h"
#import "StyloMapViewController.h"
#import "StyloNavigationViewController.h"

@interface StyloStoreLocatorViewController ()

@end

@implementation StyloStoreLocatorViewController{
    NSMutableArray *storeData;
    NSMutableArray *storeSearch;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _statusLabel.text=@"Nearby Stores";
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"maps" ofType:@"json"];

    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSArray *arr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    storeData = [[NSMutableArray alloc] initWithArray:arr];
    
    storeSearch = [[NSMutableArray alloc] init];
    storeSearch = [storeData mutableCopy];
    
    currentLocation = [[CLLocation alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    [self performSelector:@selector(stopLocationManager) withObject:nil afterDelay:1.0];
    
   /* UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(revealMenu)];
    [self.navigationItem setRightBarButtonItem:barButton];
    */
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    currentLocation = [locations lastObject];
}
-(void)stopLocationManager{
    [locationManager stopUpdatingLocation];
    for (int i=0; i<[storeData count];i++){
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:[[[storeData objectAtIndex:i] objectForKey:@"Latitude"] doubleValue] longitude:[[[storeData objectAtIndex:i] objectForKey:@"Longitude"] doubleValue] ];
        
        CLLocationDistance distance = [locA distanceFromLocation:currentLocation];
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[storeData objectAtIndex:i];
        [newDict addEntriesFromDictionary:oldDict];
        //NSString *distString = [NSString stringWithFormat:@"%.2f",distance/1000];
        NSNumber *distanceNumber = [NSNumber numberWithFloat:distance/1000];
        [newDict setObject:distanceNumber forKey:@"distance"];
        [storeData replaceObjectAtIndex:i withObject:newDict];
    }
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    
    NSArray *sortedStores = [storeData sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    [storeData removeAllObjects];
    storeData = [sortedStores mutableCopy];
    
    storeSearch = [storeData mutableCopy];
    [_storeTable reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table View Data Source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [storeSearch count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"storeCell"];
    
    UILabel *city = (UILabel *)[cell viewWithTag:1];
    UILabel *branch = (UILabel *)[cell viewWithTag:2];
    UILabel *distance = (UILabel *)[cell viewWithTag:3];
    
    city.text = [[storeSearch objectAtIndex:indexPath.row] objectForKey:@"City"];
    branch.text = [[storeSearch objectAtIndex:indexPath.row] objectForKey:@"Name"];
    distance.text = [NSString stringWithFormat:@"%.2f KM",[[[storeSearch objectAtIndex:indexPath.row] objectForKey:@"distance"] floatValue]];
    
    return cell;
}

#pragma mark - Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    StyloMapViewController *destination = (StyloMapViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"mapView"];
    destination.stores = storeData;
    destination.selectedStore = [storeSearch objectAtIndex:indexPath.row];
    [self presentViewController:destination animated:YES completion:nil];
}

#pragma mark - Search Bar Delegate

- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [_storeTable setUserInteractionEnabled:NO];
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if ([_searchBar isFirstResponder] && [touch view] != _searchBar)
    {
        [_searchBar resignFirstResponder];
        [_storeTable setUserInteractionEnabled:YES];
    }
    [super touchesBegan:touches withEvent:event];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [storeSearch removeAllObjects];
    if([searchText  isEqual:@""]){
        
        _statusLabel.text = @"Nearby Stores";
        storeSearch = [storeData mutableCopy];
        
    } else {
        _statusLabel.text = @"Search Result";
        
        for (int i=0; i<[storeData count]; i++)
        {
            NSString *cityString = [[[storeData objectAtIndex:i] objectForKey:@"City"] lowercaseString];
            NSString *searchString = [searchText lowercaseString];
            if([cityString hasPrefix:searchString]){
                [storeSearch addObject:[storeData objectAtIndex:i]];
            }
        }
    }
    [_storeTable reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
