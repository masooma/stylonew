//
//  StyloShoppingCartViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 30/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloShoppingCartViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *shoppingCartTableView;
@property (strong, nonatomic) IBOutlet UILabel *totalLabel;
@end
