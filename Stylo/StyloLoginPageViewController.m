//
//  StyloLoginPageViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 30/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloLoginPageViewController.h"
#import "StyloNavigationViewController.h"
#import "StyloAddressFormViewController.h"
#import "MBProgressHUD.h"
#import "StyloAppDelegate.h"
#import "StyloAddressListViewController.h"

@interface StyloLoginPageViewController ()

@end

@implementation StyloLoginPageViewController{
    UIColor *styloPrimaryColor;
    UIColor *styloSecondaryColor;
    MBProgressHUD *hud;
    NSMutableURLRequest *loginRequest;
    StyloAppDelegate *appDelegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(_isCheckOut){
        [_createAccountButton setTitle:@"Guest Checkout" forState:UIControlStateNormal];
        NSLog(@"%@", _checkoutData);
    }
    appDelegate = [[UIApplication sharedApplication] delegate];
    self.navigationItem.title=@"Login";
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(revealMenu)];
    [self.navigationItem setRightBarButtonItem:barButton];
    
    CGRect frameRect = _emailField.frame;
    CGRect frameRect2 = _passwordField.frame;
    frameRect.size.height = 40;
    frameRect2.size.height = 40;
    _emailField.frame = frameRect;
    _passwordField.frame=frameRect2;
   /* _emailField.leftView =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"userIcon"]];
    _emailField.leftViewMode = UITextFieldViewModeAlways;
    _passwordField.leftView =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"passwordIcon"]];
    _passwordField.leftViewMode = UITextFieldViewModeAlways;
    */
    styloPrimaryColor = [UIColor colorWithRed:92/255.0 green:45/255.0 blue:145/255.0 alpha:1];
    styloSecondaryColor = [UIColor colorWithRed:243/255.0 green:145/255.0 blue:188/255.0 alpha:1];
  /*  for(id x in [self.view subviews]){
        if([x isKindOfClass:[UITextField class]])
        {
            UITextField *textField = x;
            textField.layer.borderColor = [styloSecondaryColor CGColor];
            textField.layer.borderWidth = 1.f;
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: styloSecondaryColor}];
            textField.layer.sublayerTransform = CATransform3DMakeTranslation(5.5, 0, 0);
            textField.delegate=self;
        }
    }
    */
}
-(void)viewDidAppear:(BOOL)animated{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)logMeIn{
    NSURL *targetUrl;
    targetUrl = [NSURL URLWithString:@"http://54.179.143.160/login.php"];
    loginRequest = [NSMutableURLRequest requestWithURL:targetUrl ];
    
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",_emailField.text,_passwordField.text];
 
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [loginRequest setHTTPMethod:@"POST"];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[post length]];
    [loginRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [loginRequest setHTTPBody:postData];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:loginRequest delegate:self];
    [connection start];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Logging In";
}

-(void)shakeItBaby:(UIView *)viewToShake{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.07];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([viewToShake center].x - 5.0f, [viewToShake center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([viewToShake center].x + 5.0f, [viewToShake center].y)]];
    
    [viewToShake.layer addAnimation:animation forKey:@"key"];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(void)attemptLogin{
    if(![self NSStringIsValidEmail:_emailField.text])
    {
        [self shakeItBaby:_emailField];
        [_emailField becomeFirstResponder];
    }else if(_passwordField.text.length==0)
    {
        [self shakeItBaby:_passwordField];
        [_passwordField becomeFirstResponder];
    }else{
        [self logMeIn];
    }
}
-(void)loggedInSuccessfullyWithMessage:(NSString *)message{
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
	
	// Set custom view mode
	hud.mode = MBProgressHUDModeCustomView;
	
	hud.labelText = @"Logged In";
    [self performSelector:@selector(goToRoot) withObject:nil afterDelay:1.5];
}
-(void)goToRoot{
	[hud hide:YES];
    appDelegate.isLoggedIn = YES;
    [self.navigationController performSelector:@selector(reloadMenu) withObject:nil];
    if(_isCheckOut){
        StyloAddressListViewController *billingAddressList = (StyloAddressListViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"shippingView"];
        billingAddressList.isShipping = NO;
        billingAddressList.checkoutData = _checkoutData;
        [self.navigationController performSelector:@selector(replaceLastWith:) withObject:billingAddressList];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
-(void)loginFailedWithErrorMessage:(NSString *)message{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error Logging In", @"")
                                message:message
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
    [hud hide:YES];
}
#pragma mark - Text Field Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == (UITextField *)[self.view viewWithTag:100]){
        [[self.view viewWithTag:101] becomeFirstResponder];
    }else if (textField == (UITextField *)[self.view viewWithTag:101]){
        [self.view endEditing:YES];
        [self attemptLogin];
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if(textField.text.length >0){
        textField.layer.borderColor = [styloSecondaryColor CGColor];
    }else{
        textField.layer.borderColor = [styloSecondaryColor CGColor];
    }
    textField.layer.borderWidth = 1.0f;
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.clipsToBounds = NO;
    
    textField.layer.borderColor = [styloPrimaryColor CGColor];
    textField.layer.borderWidth = 2.0f;
    
    return YES;
}
#pragma mark - Interface Buider Actions
- (IBAction)login:(id)sender {
    [self attemptLogin];
}

- (IBAction)stopEditingIt:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)createAccount:(id)sender {
    if(_isCheckOut)
    {
        StyloAddressFormViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"billingInfoView"];
        destination.checkoutData = _checkoutData;
        [self.navigationController performSelector:@selector(replaceLastWith:) withObject:destination];
    }else{
        
        UIViewController *destinationViewController;
        destinationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"signUpView"];
        [self.navigationController performSelector:@selector(replaceLastWith:) withObject:destinationViewController];
    }
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
#pragma mark - Connection Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {

    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    if([[response objectForKey:@"code"] isEqual:@"100"]){
        [self loggedInSuccessfullyWithMessage:[response objectForKey:@"message"]];
    }else{
        [self loginFailedWithErrorMessage:[response objectForKey:@"message"]];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [hud hide:YES];
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
    [hud hide:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // Do anything you want with it
    
}
@end
