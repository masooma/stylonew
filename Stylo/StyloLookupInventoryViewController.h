//
//  StyloLookupInventoryViewController.h
//  Stylo
//
//  Created by iOS Department-Pantera on 21/10/15.
//  Copyright (c) 2015 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloLookupInventoryViewController : UIViewController
@property NSDictionary *item;
@end
